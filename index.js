console.log("Hello Word!");

// [section] Aritmetic operations
	// basic assignment operator (=)
	// the assignment operator assigns the value of the right hand to a variable

	let assignmentNumber = 8;
	console.log("The value of the assignmentNumber variable" + assignmentNumber);

let x = 200;
let y = 18;

console.log("x: " + x);
console.log("y: " + y);

// Addition
let sum = x + y;
console.log("Result of addition operator:" +  sum);

//subtraction
let difference = x - y;
console.log("result of subtraction operator:" + difference);

//product
let product = x * y;
console.log("result of multiplication operator:" + product);

//quotient
let quotient = x - y;
console.log("result of division operator:" + quotient);

let modulo = x % y;
console.log("Result of modulo operator:" + modulo);

// Continuation of assigment operator

// Addition assigment operator

	// assigmentNumber = assignmentNumber + 2; // long method
	// console.log(assigmentNumber);

	// short method
	assignmentNumber += 2;
	console.log("result of addition assigment operator: " + assignmentNumber);

	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber);

// subtraction assignment operator
	assignmentNumber -= 3;
	console.log("result of subtraction assignment operator: " + assignmentNumber);

// multiplication assignment operator
	assignmentNumber *= 2;
	console.log("result of multiplication assignment operator: " + assignmentNumber);

// division assignment operator
	assignmentNumber /= 3;
	console.log("result of division assignment operator: " + assignmentNumber);

// [section] PEMDAS	(order of operations)
// Multiple Operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = (1+(2-3)) * (4/5);
console.log(pemdas);

//heirarchy
//combinations of multiple arithmetic operators

// [SECTION] Increment and decrement

// operators that add or subtract values by 1 and reassign the value of the variable where the increment and decrement was applied.


let z = 1;
	// pre-increment
	let increment = ++z;
	console.log("Result of the pre-increment:" + increment);
	console.log("Result of pre-increment of z: " + z);

	//post-increment
 	increment = z++;
	console.log("Result of post-increment:" + increment);
	console.log("Result of post-increment of z: " + z);


	//pre decrement
	let decrement = --z;
	console.log("result of pre-decrement: " + decrement);
	console.log("result of pre-decrement of z: " + z);

	//post-decrement
	decrement = z--;
	console.log("result of post-decrement: " + decrement);
	console.log("result of post-decrement of z: " + z);


// [section] type coercion
// is the automatic conversion of values from one data type to another

let numA = 10;
let numB ="12";

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

//number and number data type will result to number
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion);


// combination of number and boolean data type will result to number
let numX = 10;
let numY = true;
	// boolean values are
		// true = 1;
		// false = 0;
coercion = numX + numY;
console.log(coercion);
console.log(typeof coercion);


// [section] comparison operators
// Comparison operators are used to evaluate and compare the left and right operands
// after evaluation, it returns a boolean value


//equality operator
// compares the value, but not the data type
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true

let juan = "juan";
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //false //quality operators is strict with letter casing
console.log(juan == "juan"); //true

//inequality operator <!=>
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false

//[SECTION]	Relational Operators
// Some comparison operators to check whether one value is greater or less than the other value
// it will return a value of true or false

console.log("------------")
let a = 50;
let b = 65;

//GT or Greater than operator (>)
let isGreaterThan = a > b; //50 < 65 = false
console.log(isGreaterThan);

//LT or Less than operator (<)
let isLessThan = a < b; //50 < 65 = true
console.log(isLessThan);

//GTE or Greater than or equal (>=)
let isGTorEqual = a  >= b; //false
console.log(isGTorEqual);

//LTE or less than or equal (<=)
let isLTEorEqual = a  <= b; //true
console.log(isLTEorEqual);

// Logical and operator


let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("result of logical AND operator: " + allRequirementsMet); //false

//logical OR operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("result of logical OR operator: " + someRequirementsMet); //true

// Logical not operator (!)
// Returns the opposite value

let someRequirementsNotMet = !isLegalAge; //false
console.log("Result of logical Not Operator:" + someRequirementsNotMet)
